<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\DanhSachTaiKhoan;
use App\Models\Status;
use Illuminate\Http\Request;

class OntapController extends Controller
{
    public function createTK(Request $request)
    {
        $data = $request->all();

        DanhSachTaiKhoan::create($data);

        return response()->json([
            'status'    => 1,
            'message'   => 'Thêm tài khoản thành công!',
        ]);
    }

    public function updateTK(Request $request)
    {
        $data = $request->all();

        $taiKhoan = DanhSachTaiKhoan::find($data['id']);
 2
        if ($taiKhoan) {
            $taiKhoan->update($data);
            return response()->json([
                'status'    => 1,
                'message'   => 'Cập nhật khoản thành công!',
            ]);
        }
    }

    public function deleteTK(Request $request)
    {
        $taiKhoan = DanhSachTaiKhoan::find($request->id);

        if ($taiKhoan) {
            $taiKhoan->delete();

            return response()->json([
                'status'    => 1,
                'message'   => 'Đã xóa tài khoản thành công!',
            ]);
        }
    }

    public function dataTK()
    {
        $data = DanhSachTaiKhoan::get();

        return response()->json([
            'data'    => $data,
        ]);
    }

    public function createBaiDang(Request $request)
    {
        $data = $request->all();

        Status::create($data);

        return response()->json([
            'status'    => 1,
            'message'   => 'Đã đăng bài thành công!',
        ]);
    }

    public function dataBaiDang()
    {
        $data = Status::join('danh_sach_tai_khoans', 'danh_sach_tai_khoans.id', 'statuses.id_user')
            ->select('statuses.*', 'danh_sach_tai_khoans.ten_dang_nhap')
            ->get();

        return response()->json([
            'data'    => $data,
        ]);
    }

    public function deleteBaiDang(Request $request)
    {
        $baiDang = Status::find($request->id);

        if ($baiDang) {
            $baiDang->delete();
            return response()->json([
                'status'    => 1,
                'message'   => 'Xóa bài đăng thành công!',
            ]);
        }
    }

    public function postComment(Request $request)
    {
        $data['id_user'] = 1;
        $data['id_status'] = $request->id;
        $data['content'] = $request->content;

        Comment::create($data);

        return response()->json([
            'status'    => 1,
            'message'   => 'Đã bình luận bài viết!',
        ]);
    }

    public function dataComment()
    {
        $data = Comment::join('danh_sach_tai_khoans', 'danh_sach_tai_khoans.id', 'comments.id_user')
            ->select('comments.*', 'danh_sach_tai_khoans.ten_dang_nhap')
            ->get();

        return response()->json([
            'data'    => $data,
        ]);
    }

    public function updateBaiDang(Request $request)
    {
        $data = $request->all();

        $baiDang = Status::find($data['id']);

        if ($baiDang) {
            $baiDang->update($data);
            return response()->json([
                'status'    => 1,
                'message'   => 'Cập nhật bài viết thành công!',
            ]);
        }
    }
}
