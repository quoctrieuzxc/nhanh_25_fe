<?php

use App\Http\Controllers\BanController;
use App\Http\Controllers\ChucVuController;
use App\Http\Controllers\DanhMucController;
use App\Http\Controllers\KhuVucController;
use App\Http\Controllers\MonAnController;
use App\Http\Controllers\NguyenLieuController;
use App\Http\Controllers\NhaCungCapController;
use App\Http\Controllers\NhanVienController;
use App\Http\Controllers\NhapKhoController;
use App\Http\Controllers\OntapController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Chia các nhóm ra như sau
// Nhân Viên
Route::group(['prefix'  =>  '/admin'], function() {
    // Những gì của danh mục thì ta sẽ nhét ở group này
    Route::group(['prefix'  =>  '/danh-muc'], function() {
        // Lấy dữ liệu  -> get
        Route::get('/lay-du-lieu', [DanhMucController::class, 'getData']);
        Route::post('/tim-danh-muc', [DanhMucController::class, 'searchDanhMuc']);
        Route::post('/tao-danh-muc', [DanhMucController::class, 'createDanhMuc']);
        Route::delete('/xoa-danh-muc/{id}', [DanhMucController::class, 'xoaDanhMuc']);


    });

    // Những gì của nhân viên thì ta sẽ nhét ở group này
    Route::group(['prefix'  =>  '/nhan-vien'], function() {
        // Lấy dữ liệu  -> get
        Route::get('/lay-du-lieu', [NhanVienController::class, 'getData']);
        Route::post('/tim-nhan-vien', [NhanVienController::class, 'searchNhanVien']);
        Route::post('/tao-nhan-vien', [NhanVienController::class, 'createNhanVien']);
        Route::delete('/xoa-nhan-vien/{id}', [NhanVienController::class, 'xoaNhanVien']);

    });

    // Những gì của chức vụ thì ta sẽ nhét ở group này
    Route::group(['prefix'  =>  '/chuc-vu'], function() {
        // Lấy dữ liệu  -> get
        Route::get('/lay-du-lieu', [ChucVuController::class, 'getData']);
        Route::post('/tim-chuc-vu', [ChucVuController::class, 'searchChucVu']);
        Route::post('/tao-chuc-vu', [ChucVuController::class, 'createChucVu']);
        Route::delete('/xoa-chuc-vu/{id}', [ChucVuController::class, 'xoaChucVu']);

    });

    // Những gì của chức vụ thì ta sẽ nhét ở group này
    Route::group(['prefix'  =>  '/ban'], function() {
        // Lấy dữ liệu  -> get
        Route::get('/lay-du-lieu', [BanController::class, 'getData']);
        Route::post('/tim-ban', [BanController::class, 'searchBan']);
        Route::post('/tao-ban', [BanController::class, 'createBan']);
        Route::delete('/xoa-ban/{id}', [BanController::class, 'xoaBan']);

    });

    // Những gì của chức vụ thì ta sẽ nhét ở group này
    Route::group(['prefix'  =>  '/khu-vuc'], function() {
        // Lấy dữ liệu  -> get
        Route::get('/lay-du-lieu', [KhuVucController::class, 'getData']);
        Route::get('/lay-du-lieu-hoat-dong', [KhuVucController::class, 'getDataHoatDong']);
        Route::post('/tim-khu-vuc', [KhuVucController::class, 'searchKhuVuc']);
        Route::post('/tao-khu-vuc', [KhuVucController::class, 'createKhuVuc']);
        Route::delete('/xoa-khu-vuc/{id}', [KhuVucController::class, 'xoaKhuVuc']);
        Route::put('/cap-nhat-khu-vuc', [KhuVucController::class, 'capNhatKhuVuc']);
        Route::put('/doi-trang-thai', [KhuVucController::class, 'doiTrangThaiKhuVuc']);
    });

    // Những gì của chức vụ thì ta sẽ nhét ở group này
    Route::group(['prefix'  =>  '/nguyen-lieu'], function() {
        // Lấy dữ liệu  -> get
        Route::get('/lay-du-lieu', [NguyenLieuController::class, 'getData']);
        Route::post('/tim-nguyen-lieu', [NguyenLieuController::class, 'searchNguyenLieu']);
        Route::post('/tao-nguyen-lieu', [NguyenLieuController::class, 'createNguyenLieu']);
        Route::delete('/xoa-nguyen-lieu/{id}', [NguyenLieuController::class, 'xoaNguyenLieu']);

    });

    Route::group(['prefix'  =>  '/nhap-kho'], function() {
        Route::get('/lay-du-lieu', [NhapKhoController::class, 'getData']);
        Route::post('/them-nguyen-lieu', [NhapKhoController::class, 'addNguyenLieu']);
        Route::delete('/xoa-nguyen-lieu/{id}', [NhapKhoController::class, 'xoaNguyenLieu']);


    });
    Route::group(['prefix'  =>  '/nha-cung-cap'], function() {
        // Lấy dữ liệu  -> get
        Route::get('/lay-du-lieu', [NhaCungCapController::class, 'getData']);
        Route::post('/tim-nha-cung-cap', [NhaCungCapController::class, 'searchNhaCungCap']);
        Route::post('/tao-nha-cung-cap', [NhaCungCapController::class, 'createNhaCungCap']);
        Route::delete('/xoa-nha-cung-cap/{id}', [NhaCungCapController::class, 'xoaNhaCungCap']);

    });
    Route::group(['prefix'  =>  '/mon-an'], function() {
        // Lấy dữ liệu  -> get
        Route::get('/lay-du-lieu', [MonAnController::class, 'getData']);
        Route::post('/tim-mon-an', [MonAnController::class, 'searchMonAn']);
        Route::post('/tao-mon-an', [MonAnController::class, 'createMonAn']);
        Route::delete('/xoa-mon-an/{id}', [MonAnController::class, 'xoaMonAn']);

    });

    Route::group(['prefix'  =>  '/tai-khoan'], function() {
        Route::post('/create', [OntapController::class, 'createTK']);
        Route::post('/update', [OntapController::class, 'updateTK']);
        Route::post('/delete', [OntapController::class, 'deleteTK']);
        Route::get('/data', [OntapController::class, 'dataTK']);
    });

    Route::group(['prefix'  =>  '/bai-dang'], function() {
        Route::post('/create', [OntapController::class, 'createBaiDang']);
        Route::post('/update', [OntapController::class, 'updateBaiDang']);
        Route::post('/delete', [OntapController::class, 'deleteBaiDang']);
        Route::post('/comment', [OntapController::class, 'postComment']);
        Route::get('/comment-data', [OntapController::class, 'dataComment']);
        Route::get('/data', [OntapController::class, 'dataBaiDang']);
    });
});

